# Specify the provider and access details
provider "aws" {
  region = "${var.aws_region}"
  profile = "default"
}


# # Create a VPC to launch our instances into
# resource "aws_vpc" "default" {
#   cidr_block = "10.0.0.0/16"
# }

# # Create an internet gateway to give our subnet access to the outside world
# resource "aws_internet_gateway" "default" {
#   vpc_id = "${aws_vpc.default.id}"
# }

# # Grant the VPC internet access on its main route table
# resource "aws_route" "internet_access" {
#   route_table_id         = "${aws_vpc.default.main_route_table_id}"
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = "${aws_internet_gateway.default.id}"
# }

# # Create a subnet to launch our instances into
# resource "aws_subnet" "default" {
#   vpc_id                  = "${aws_vpc.default.id}"
#   cidr_block              = "10.0.1.0/24"
#   map_public_ip_on_launch = true
# }

# # A security group for the ELB so it is accessible via the web
# resource "aws_security_group" "elb" {
#   name        = "terraform_example_elb"
#   description = "Used in the terraform"
#   vpc_id      = "${aws_vpc.default.id}"

#   # HTTP access from anywhere
#   ingress {
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   # outbound internet access
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

# # Our default security group to access
# # the instances over SSH and HTTP
# resource "aws_security_group" "default" {
#   name        = "terraform_example"
#   description = "Used in the terraform"
#   vpc_id      = "${aws_vpc.default.id}"

#   # SSH access from anywhere
#   ingress {
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   # HTTP access from the VPC
#   ingress {
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["10.0.0.0/16"]
#   }

#   # outbound internet access
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

# resource "aws_elb" "web" {
#   name = "terraform-example-elb"

#   subnets         = ["${aws_subnet.default.id}"]
#   security_groups = ["${aws_security_group.elb.id}"]
#   instances       = ["${aws_instance.web.id}"]

#   listener {
#     instance_port     = 80
#     instance_protocol = "http"
#     lb_port           = 80
#     lb_protocol       = "http"
#   }
# }

# # resource "aws_key_pair" "auth" {
# #   key_name   = "${var.key_name}"
# #   public_key = "${file(var.public_key_path)}"
# # }

# resource "aws_key_pair" "auth" {
#   key_name   = "demo2_1"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCXWx4EjK1+3Fh1biaqZolCcyBxhBpsOsTufNaW2Phj9QxiERUXx0TK8CRqnPXPfqyP/9HeyK/u84DU7thEtRQt4Y/1A9kRoARKqMFObLCKSrEewhXQ5OrLj2lTaSB+Npc31vRb5pB2RYoEdzaEIfSf4F2gd8Io/y61IO1BSOct+DYyds8sYMeJ7ceiRfTWppKKL9BGVYMgs9w1bcG4pUPQ7W/DnUSYPfiHUVQ9140vb/APoIT/EaxJDFsPWXe6/Bl/P7u8jziqCw37PKqRcxpXtcJSFEZzo69vLYFquewaxuJVhIwEoTidvdyxOlDd8gYXFKSFrmD/7Sjj4QBx4JSx"
# }


# resource "aws_instance" "web" {
#   # The connection block tells our provisioner how to
#   # communicate with the resource (instance)
#   connection {
#     # The default username for our AMI
#     user = "ec2-user"
#     host = "${self.public_ip}"
#     private_key = var.TF_VAR_pk
 
#     # The connection will use the local SSH agent for authentication.
#   }

#   instance_type = "t2.micro"

#   # Lookup the correct AMI based on the region
#   # we specified
#   ami = "${lookup(var.aws_amis, var.aws_region)}"

#   # The name of our SSH keypair we created above.
#   key_name = "${aws_key_pair.auth.id}"

#   # Our Security group to allow HTTP and SSH access
#   vpc_security_group_ids = ["${aws_security_group.default.id}"]

#   # We're going to launch into the same subnet as our ELB. In a production
#   # environment it's more common to have a separate private subnet for
#   # backend instances.
#   subnet_id = "${aws_subnet.default.id}"

#   # We run a remote provisioner on the instance after creating it.
#   # In this case, we just install nginx and start it. By default,
#   # this should be on port 80
#   provisioner "remote-exec" {
#     inline = [
#       "sudo yum install -y update",
#       "sudo yum install -y nginx",
#       "sudo service nginx start",
#     ]
#   }
# }
# resource "aws_kms_key" "mykey" {
#   description             = "This key is used to encrypt bucket objects"
#   deletion_window_in_days = 10
# }


#  resource "aws_s3_bucket" "b" {
#    bucket = "my-tf-test-bucket-tf2019"
# #   server_side_encryption_configuration {
# #     rule {
# #       apply_server_side_encryption_by_default {
# #         kms_master_key_id = "${aws_kms_key.mykey.arn}"
# #         sse_algorithm     = "aws:kms"
# #       }
# #     }
# #   }
#  }

#  resource "aws_s3_bucket_policy" "b" {
#    bucket = "${aws_s3_bucket.b.id}"

#    policy = <<POLICY
#  {
#    "Version": "2012-10-17",
#    "Id": "MYBUCKETPOLICY",
#    "Statement": [
#      {
#        "Sid": "IPAllow",
#        "Effect": "Deny",
#        "Principal": "*",
#        "Action": "s3:*",
#        "Resource": "arn:aws:s3:::my-tf-test-bucket-tf2019/*"
#      }
#    ]
#  }
#  POLICY
#  }


# module "s3-app" {
#  source = "./modules/app"
# }

# module "s3-app" {
#  source = "app.terraform.io/awsrcs/s3pmr/aws//modules/app"
#  version = "~> 2.0.0"
#}