variable "key_name" {
  description = "Desired name of AWS key pair"
  default = "demo2_1"
}

#variable "TF_VAR_pk" {
#  type = string
#}


variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "us-east-1"
}

# Linux Precise 12.04 LTS (x64)
variable "aws_amis" {
  default = {
    eu-west-1 = "ami-674cbc1e"
    us-east-1 = "ami-00eb20669e0990cb4"
    us-west-1 = "ami-969ab1f6"
    us-west-2 = "ami-8803e0f0"
  }
}
